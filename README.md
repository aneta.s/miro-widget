## Miro tech assignment

Core requirements of the test task are implemented.

### Build
```
mvn clean install
```

### Run
```
mvn spring-boot:run
```
Widget service is available at `localhost:8080/api/widget`.

REST API specs are at `http://localhost:8080/swagger-ui/index.html?configUrl=/v3/api-docs/swagger-config`.
