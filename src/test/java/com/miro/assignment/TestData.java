package com.miro.assignment;

import com.miro.assignment.models.ImmutableWidget;
import com.miro.assignment.models.ImmutableWidgetRequest;
import com.miro.assignment.models.Widget;
import com.miro.assignment.models.WidgetRequest;
import com.miro.assignment.util.DateTimeProvider;
import org.springframework.util.IdGenerator;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.UUID;


public enum TestData {
    ;
    public static final String ID = "id";
    public static final long Z_INDEX = 5;
    public static final DateTimeProvider FIXED_TIME_PROVIDER = new FixedTimeProvider();
    public static final UUID FIXED_UUID = UUID.nameUUIDFromBytes(ID.getBytes());
    public static final IdGenerator FIXED_ID_GENERATOR = () -> FIXED_UUID;
    public static final ZonedDateTime MODIFIED = ZonedDateTime.ofInstant(Instant.ofEpochMilli(1), ZoneOffset.UTC);
    public static final WidgetRequest WIDGET_REQUEST = ImmutableWidgetRequest.builder()
            .x(1)
            .y(2)
            .height(3)
            .width(4)
            .build();
    public static final Widget WIDGET = ImmutableWidget.builder()
            .id(ID)
            .x(1)
            .y(2)
            .height(3)
            .width(4)
            .zIndex(Z_INDEX)
            .modified(FIXED_TIME_PROVIDER.now())
            .build();

    public static Widget testWidget(long aZIndex) {
        return testWidget(ID, aZIndex);
    }

    public static Widget testWidget(String aId, long aZIndex) {
        return ImmutableWidget.builder()
                .id(aId)
                .x(1)
                .y(2)
                .width(3)
                .height(4)
                .zIndex(aZIndex)
                .modified(MODIFIED)
                .build();
    }

    private static class FixedTimeProvider implements DateTimeProvider {
        @Override
        public ZonedDateTime now() {
            return ZonedDateTime.ofInstant(Instant.ofEpochMilli(1), ZoneId.systemDefault());
        }
    }
}
