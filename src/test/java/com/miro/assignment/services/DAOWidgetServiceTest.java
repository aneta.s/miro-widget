package com.miro.assignment.services;

import com.miro.assignment.dao.WidgetDAO;
import com.miro.assignment.exception.EntityNotFoundException;
import com.miro.assignment.models.ImmutableWidgetRequest;
import com.miro.assignment.models.WidgetConverter;
import com.miro.assignment.models.WidgetRequest;
import org.junit.Before;
import org.junit.Test;

import static com.miro.assignment.TestData.FIXED_ID_GENERATOR;
import static com.miro.assignment.TestData.FIXED_UUID;
import static com.miro.assignment.TestData.ID;
import static com.miro.assignment.TestData.WIDGET;
import static com.miro.assignment.TestData.WIDGET_REQUEST;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class DAOWidgetServiceTest {
    private final WidgetDAO theDAO = mock(WidgetDAO.class);
    private final WidgetConverter theConverter = mock(WidgetConverter.class);
    private DAOWidgetService theDAOWidgetService;

    @Before
    public void setUp() {
        theDAOWidgetService = new DAOWidgetService(theDAO, theConverter, FIXED_ID_GENERATOR);
    }

    @Test(expected = EntityNotFoundException.class)
    public void throwsWhenUpdatingNonExistentWidget() {
        when(theDAO.getById(ID)).thenReturn(null);
        theDAOWidgetService.update(ID, WIDGET_REQUEST);
    }

    @Test(expected = EntityNotFoundException.class)
    public void throwsWhenRetrievingByIdNonExistentWidget() {
        when(theDAO.getById(ID)).thenReturn(null);
        theDAOWidgetService.getById(ID);
    }

    @Test
    public void generatesIdForCreatedWidget() {
        theDAOWidgetService.create(WIDGET_REQUEST);
        verify(theConverter).fromRequest(eq(WIDGET_REQUEST), eq(FIXED_UUID.toString()), anyLong());
    }

    @Test
    public void setsCorrectZIndexIfNotProvidedInUpdateRequest() {
        when(theDAO.getById(anyString())).thenReturn(WIDGET);
        theDAOWidgetService.update(FIXED_UUID.toString(), WIDGET_REQUEST);

        verify(theConverter).fromRequest(eq(WIDGET_REQUEST), eq(FIXED_UUID.toString()), eq(1L));
    }

    @Test
    public void setsCorrectZIndexIfNotProvidedInCreateRequest() {
        theDAOWidgetService.create(WIDGET_REQUEST);
        verify(theConverter).fromRequest(eq(WIDGET_REQUEST), eq(FIXED_UUID.toString()), eq(1L));
    }

    @Test
    public void usesZIndexFromRequestIfProvided() {
        long myZIndex = 3;
        WidgetRequest myRequest = ImmutableWidgetRequest.builder()
                .from(WIDGET_REQUEST)
                .zindex(myZIndex)
                .build();

        theDAOWidgetService.create(myRequest);

        verify(theConverter).fromRequest(eq(myRequest), eq(FIXED_UUID.toString()), eq(myZIndex));
    }
}
