package com.miro.assignment.controller;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.miro.assignment.WidgetApplication;
import com.miro.assignment.exception.EntityNotFoundException;
import com.miro.assignment.models.ImmutableWidget;
import com.miro.assignment.models.Widget;
import com.miro.assignment.services.WidgetService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.List;

import static com.miro.assignment.TestData.ID;
import static com.miro.assignment.TestData.testWidget;
import static com.miro.assignment.controller.WidgetController.BASE_PATH;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        classes = WidgetApplication.class)
@AutoConfigureMockMvc
public class WidgetControllerTest {

    @Autowired
    private MockMvc theMockMvc;

    @Autowired
    private ObjectMapper theObjectMapper;

    @MockBean
    private WidgetService theWidgetService;

    @Test
    public void whenWidgetsExistThenReturnThemAll() throws Exception {
        List<Widget> myExpected = List.of(
                testWidget("A", 5),
                testWidget("B", 7));
        given(theWidgetService.getAll()).willReturn(myExpected);

        MvcResult myResult = theMockMvc.perform(get(BASE_PATH)
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andReturn();

        List<Widget> myActual = theObjectMapper.readerFor(ImmutableWidget.class)
                .<Widget>readValues(myResult.getResponse().getContentAsByteArray())
                .readAll();

        assertThat(myActual)
                .usingElementComparatorIgnoringFields("modified")
                .containsExactlyElementsOf(myExpected);
    }

    @Test
    public void whenWidgetExistsThenGetByIdReturnsIt() throws Exception {
        Widget myExpected = testWidget(ID, 5);
        given(theWidgetService.getById(ID)).willReturn(myExpected);

        MvcResult myResult = theMockMvc.perform(get(BASE_PATH + "/" + ID)
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andReturn();

        Widget myActual = theObjectMapper.readerFor(ImmutableWidget.class)
                .readValue(myResult.getResponse().getContentAsByteArray());

        assertWidgets(myActual, myExpected);
    }

    @Test
    public void whenWidgetDoesNotExistThenGetByIdReturns404() throws Exception {
        given(theWidgetService.getById(eq(ID))).willThrow(new EntityNotFoundException(""));

        theMockMvc.perform(get(BASE_PATH + "/" + ID)
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound())
                .andReturn();
    }

    @Test
    public void whenCreatingValidWidgetThenReturn200() throws Exception {
        ObjectNode myRequest = JsonNodeFactory.instance.objectNode();
        myRequest.put("x", 0)
                .put("y", 0)
                .put("width", 10)
                .put("height", 10);

        theMockMvc.perform(post(BASE_PATH)
                .content(theObjectMapper.writeValueAsBytes(myRequest))
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void whenCreatingInvalidWidgetThenReturn400() throws Exception {
        JsonNode myRequest = widgetRequest(0, 10);
        theMockMvc.perform(post(BASE_PATH)
                .content(theObjectMapper.writeValueAsBytes(myRequest))
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    @Test
    public void whenUpdatingValidWidgetThenReturn200() throws Exception {
        JsonNode myRequest = widgetRequest(10, 10);
        theMockMvc.perform(put(BASE_PATH + "/" + ID)
                .content(theObjectMapper.writeValueAsBytes(myRequest))
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void whenUpdatingNonExistentWidgetThenReturn400() throws Exception {
        given(theWidgetService.update(eq(ID), any())).willThrow(new EntityNotFoundException(""));

        JsonNode myRequest = widgetRequest(10, 10);
        theMockMvc.perform(put(BASE_PATH + "/" + ID)
                .content(theObjectMapper.writeValueAsBytes(myRequest))
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound())
                .andReturn();
    }

    @Test
    public void deleteReturnsNoContent() throws Exception {
        theMockMvc.perform(delete(BASE_PATH + "/" + ID)
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNoContent());
    }

    private static void assertWidgets(Widget aActual, Widget aExpected) {
        assertThat(aActual)
                .usingRecursiveComparison()
                .ignoringFields("modified")
                .isEqualTo(aExpected);
        assertThat(aActual.getModified()).isEqualTo(aExpected.getModified());
    }

    private static JsonNode widgetRequest(int aWidth, int aHeight) {
        ObjectNode myRequest = JsonNodeFactory.instance.objectNode();
        return myRequest.put("x", 0)
                .put("y", 0)
                .put("width", aWidth)
                .put("height", aHeight);
    }
}
