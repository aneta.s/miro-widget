package com.miro.assignment.dao;

import com.miro.assignment.models.Widget;
import org.junit.Before;
import org.junit.Test;

import java.util.Collection;

import static com.miro.assignment.TestData.testWidget;
import static org.assertj.core.api.Assertions.assertThat;

public class InMemoryDAOTest {
    private static final String ID = "id";
    private static final long Z_INDEX = 1;
    private static final Widget WIDGET = testWidget(Z_INDEX);
    private InMemoryDAO theDAO;

    @Before
    public void setUp() {
        theDAO = new InMemoryDAO();
    }

    @Test
    public void createsWidget() {
        Widget myActual = theDAO.create(WIDGET);

        assertThat(myActual).isEqualTo(WIDGET);
    }

    @Test
    public void createdWidgetIsInAll() {
        theDAO.create(WIDGET);

        Collection<Widget> myAll = theDAO.getAll();

        assertThat(myAll).hasSize(1);
        assertThat(myAll).containsExactly(WIDGET);
    }

    @Test
    public void canRetrieveCreatedWidgetById() {
        theDAO.create(WIDGET);

        Widget myActual = theDAO.getById(ID);

        assertThat(myActual).isEqualTo(WIDGET);
    }

    @Test
    public void updatesWidget() {
        Widget myUpdatedWidget = testWidget(4);

        theDAO.create(WIDGET);
        Widget myActual = theDAO.update(myUpdatedWidget);

        assertThat(myActual).isEqualTo(myUpdatedWidget);
    }

    @Test
    public void updatedWidgetIsInAll() {
        theDAO.create(WIDGET);
        Widget myUpdatedWidget = testWidget(4);
        theDAO.update(myUpdatedWidget);

        Collection<Widget> myAll = theDAO.getAll();

        assertThat(myAll).hasSize(1);
        assertThat(myAll).containsExactly(myUpdatedWidget);
    }

    @Test
    public void canRetrieveUpdatedWidgetById() {
        theDAO.create(WIDGET);
        Widget myUpdatedWidget = testWidget(4);
        theDAO.update(myUpdatedWidget);

        Widget myActual = theDAO.getById(ID);

        assertThat(myActual).isEqualTo(myUpdatedWidget);
    }

    @Test
    public void deletesWidget() {
        theDAO.create(WIDGET);
        Widget myDeleted = theDAO.deleteById(ID);

        assertThat(myDeleted).isNotNull();
        assertThat(theDAO.getAll()).isEmpty();
    }

    @Test
    public void shiftsZIndices() {
        createWidgets(testWidget("A", -1),
                testWidget("B", 0),
                testWidget("C", 1),
                testWidget("D", 3));

        theDAO.create(testWidget("X", 0));
        theDAO.create(testWidget("Y", -2));

        Collection<Widget> myAll = theDAO.getAll();

        assertThat(myAll).containsExactly(testWidget("Y", -2),
                testWidget("A", -1),
                testWidget("X", 0),
                testWidget("B", 1),
                testWidget("C", 2),
                testWidget("D", 3));
    }

    @Test
    public void givesCorrectHighestZIndex() {
        createWidgets(testWidget(-5),
                testWidget(-10));

        assertThat(theDAO.highestZIndex()).isEqualTo(-5);
    }

    private void createWidgets(Widget... aWidgets) {
        for (Widget myWidget : aWidgets) {
            theDAO.create(myWidget);
        }
    }
}
