package com.miro.assignment.models;

import org.junit.Test;

import static com.miro.assignment.TestData.FIXED_TIME_PROVIDER;
import static com.miro.assignment.TestData.WIDGET;
import static com.miro.assignment.TestData.WIDGET_REQUEST;
import static org.assertj.core.api.Assertions.assertThat;

public class WidgetConverterTest {
    private static final String ID = "id";
    private static final long Z_INDEX = 5;
    private final WidgetConverter theWidgetConverter = new WidgetConverter(FIXED_TIME_PROVIDER);

    @Test
    public void convertsValidWidgetRequest() {
        Widget myActual = theWidgetConverter.fromRequest(WIDGET_REQUEST, ID, Z_INDEX);
        assertThat(myActual).isEqualTo(WIDGET);
    }

    @Test(expected = NullPointerException.class)
    public void throwsWhenIdIsNull() {
        theWidgetConverter.fromRequest(WIDGET_REQUEST, null, Z_INDEX);
    }

    @Test(expected = IllegalStateException.class)
    public void throwsWhenWidthIsLessOrEqualZero() {
        WidgetRequest myWidgetRequest = ImmutableWidgetRequest.builder()
                .from(WIDGET_REQUEST)
                .width(0)
                .build();
        theWidgetConverter.fromRequest(myWidgetRequest, ID, Z_INDEX);
    }

    @Test(expected = IllegalStateException.class)
    public void throwsWhenHeightIsLessOrEqualZero() {
        WidgetRequest myWidgetRequest = ImmutableWidgetRequest.builder()
                .from(WIDGET_REQUEST)
                .height(-1)
                .build();
        theWidgetConverter.fromRequest(myWidgetRequest, ID, Z_INDEX);
    }
}
