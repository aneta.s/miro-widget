package com.miro.assignment.exception;

import org.immutables.value.Value;

@Value.Immutable
public interface ErrorInfo {
    static ErrorInfo of(String aMessage) {
        return ImmutableErrorInfo.of(aMessage);
    }

    @Value.Parameter
    String getMessage();
}
