package com.miro.assignment.exception;

public class EntityNotFoundException extends RuntimeException {
    public EntityNotFoundException(String aMessage) {
        super(aMessage);
    }
}
