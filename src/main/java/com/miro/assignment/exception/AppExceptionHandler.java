package com.miro.assignment.exception;

import com.fasterxml.jackson.databind.exc.ValueInstantiationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class AppExceptionHandler {
    private static final Logger LOGGER = LoggerFactory.getLogger(AppExceptionHandler.class);

    @ExceptionHandler(value = ValueInstantiationException.class)
    public ResponseEntity<ErrorInfo> handleInvalidRequestDefinition(Exception aException) {
        LOGGER.debug("Invalid request: {}", aException.getMessage());
        return ResponseEntity.badRequest().body(ErrorInfo.of(aException.getMessage()));
    }

    @ExceptionHandler(value = EntityNotFoundException.class)
    public ResponseEntity<ErrorInfo> handleNotFound(Exception aException) {
        LOGGER.debug("Entity not found: {}", aException.getMessage());
        return new ResponseEntity<>(ErrorInfo.of(aException.getMessage()), HttpStatus.NOT_FOUND);
    }
}
