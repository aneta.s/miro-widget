package com.miro.assignment.models;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.value.Value;

import java.time.ZonedDateTime;

@Value.Immutable
@Value.Style(jdkOnly = true)
@JsonSerialize(as = ImmutableWidget.class)
public interface Widget {
    String getId();

    long getX();

    long getY();

    int getWidth();

    int getHeight();

    long getZIndex();

    ZonedDateTime getModified();
}
