package com.miro.assignment.models;

import com.miro.assignment.util.DateTimeProvider;
import org.springframework.stereotype.Component;

@Component
public class WidgetConverter {
    private final DateTimeProvider theDateTimeProvider;

    public WidgetConverter(DateTimeProvider aDateTimeProvider) {
        theDateTimeProvider = aDateTimeProvider;
    }

    public Widget fromRequest(WidgetRequest aWidgetRequest, String aId, long aZIndex) {
        return ImmutableWidget.builder()
                .id(aId)
                .zIndex(aZIndex)
                .x(aWidgetRequest.getX())
                .y(aWidgetRequest.getY())
                .width(aWidgetRequest.getWidth())
                .height(aWidgetRequest.getHeight())
                .modified(theDateTimeProvider.now())
                .build();
    }
}
