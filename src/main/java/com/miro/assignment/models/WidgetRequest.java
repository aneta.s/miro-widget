package com.miro.assignment.models;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.immutables.value.Value;
import org.springframework.lang.Nullable;

@Value.Immutable
@JsonDeserialize(as = ImmutableWidgetRequest.class)
public interface WidgetRequest {
    long getX();

    long getY();

    int getWidth();

    int getHeight();

    @Nullable
    Long getZindex();

    @Value.Check
    default void check() {
        if (getWidth() <= 0) {
            throw new IllegalStateException("Widget width must be greater than 0");
        }
        if (getHeight() <= 0) {
            throw new IllegalStateException("Widget height must be greater than 0");
        }
    }
}
