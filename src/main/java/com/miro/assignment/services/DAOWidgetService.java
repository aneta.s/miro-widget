package com.miro.assignment.services;

import com.miro.assignment.dao.WidgetDAO;
import com.miro.assignment.exception.EntityNotFoundException;
import com.miro.assignment.models.Widget;
import com.miro.assignment.models.WidgetConverter;
import com.miro.assignment.models.WidgetRequest;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import org.springframework.util.IdGenerator;

import java.util.ArrayList;
import java.util.List;

@Component
public class DAOWidgetService implements WidgetService {
    private final WidgetDAO theWidgetDAO;
    private final WidgetConverter theWidgetConverter;
    private final IdGenerator theIdGenerator;
    private final Object theLock = new Object();

    public DAOWidgetService(WidgetDAO aWidgetDAO, WidgetConverter aWidgetConverter, IdGenerator aIdGenerator) {
        theWidgetDAO = aWidgetDAO;
        theWidgetConverter = aWidgetConverter;
        theIdGenerator = aIdGenerator;
    }

    @Override
    public Widget getById(String aId) {
        synchronized (theLock) {
            Widget myWidget = theWidgetDAO.getById(aId);
            if (myWidget != null) {
                return myWidget;
            } else {
                throw new EntityNotFoundException("Widget with id '" + aId + "' does not exist");
            }
        }
    }

    @Override
    public List<Widget> getAll() {
        synchronized (theLock) {
            return new ArrayList<>(theWidgetDAO.getAll());
        }
    }

    @Override
    public Widget create(WidgetRequest aItem) {
        synchronized (theLock) {
            return theWidgetDAO.create(theWidgetConverter.fromRequest(aItem,
                    theIdGenerator.generateId().toString(),
                    zIndexFor(aItem)));
        }
    }

    @Override
    public Widget update(String aId, WidgetRequest aUpdate) {
        synchronized (theLock) {
            ensureWidgetExists(aId);
            return theWidgetDAO.update(theWidgetConverter.fromRequest(aUpdate, aId, zIndexFor(aUpdate)));
        }
    }

    @Override
    public Widget deleteById(String aId) {
        synchronized (theLock) {
            return theWidgetDAO.deleteById(aId);
        }
    }

    private void ensureWidgetExists(String aId) {
        Widget myCurrent = theWidgetDAO.getById(aId);
        if (myCurrent == null) {
            throw new EntityNotFoundException("Widget with id '" + aId + "' does not exist");
        }
    }

    private long zIndexFor(WidgetRequest aWidget) {
        return aWidget.getZindex() == null
                ? theWidgetDAO.highestZIndex() + 1
                : aWidget.getZindex();
    }
}
