package com.miro.assignment.services;

import com.miro.assignment.models.Widget;
import com.miro.assignment.models.WidgetRequest;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface WidgetService {
    Widget getById(String id);

    List<Widget> getAll();

    Widget create(WidgetRequest aEntity);

    Widget update(String aId, WidgetRequest aUpdate);

    @Nullable
    Widget deleteById(String id);
}
