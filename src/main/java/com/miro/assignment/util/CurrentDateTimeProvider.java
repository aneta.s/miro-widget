package com.miro.assignment.util;

import java.time.ZonedDateTime;

public class CurrentDateTimeProvider implements DateTimeProvider {
    @Override
    public ZonedDateTime now() {
        return ZonedDateTime.now();
    }
}
