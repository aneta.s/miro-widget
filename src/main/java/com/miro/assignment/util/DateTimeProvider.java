package com.miro.assignment.util;

import java.time.ZonedDateTime;

@FunctionalInterface
public interface DateTimeProvider {
    ZonedDateTime now();
}
