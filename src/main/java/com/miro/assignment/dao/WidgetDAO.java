package com.miro.assignment.dao;

import com.miro.assignment.models.Widget;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface WidgetDAO {

    @Nullable
    Widget getById(String aId);

    Collection<Widget> getAll();

    Widget create(Widget aItem);

    Widget update(Widget aItem);

    @Nullable
    Widget deleteById(String aId);

    long highestZIndex();
}
