package com.miro.assignment.dao;

import com.miro.assignment.models.ImmutableWidget;
import com.miro.assignment.models.Widget;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NavigableMap;
import java.util.Objects;
import java.util.SortedMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentSkipListMap;

@Component
public class InMemoryDAO implements WidgetDAO {
    private static final Logger LOGGER = LoggerFactory.getLogger(InMemoryDAO.class);
    private final Map<String, Long> theIdToZIndex;
    private final NavigableMap<Long, Widget> theZIndexToWidget;

    public InMemoryDAO() {
        theIdToZIndex = new ConcurrentHashMap<>();
        theZIndexToWidget = new ConcurrentSkipListMap<>();
    }

    @Override
    @Nullable
    public Widget getById(String aId) {
        Long myZ = theIdToZIndex.get(aId);
        if (myZ == null) {
            return null;
        }
        return theZIndexToWidget.get(myZ);
    }

    @Override
    public Collection<Widget> getAll() {
        return theZIndexToWidget.values();
    }

    @Override
    public Widget create(Widget aItem) {
        LOGGER.debug("Creating {}", aItem);
        List<Widget> myShiftedWidgets = shiftExistingWidgets(aItem);
        theZIndexToWidget.put(aItem.getZIndex(), aItem);
        theIdToZIndex.put(aItem.getId(), aItem.getZIndex());
        myShiftedWidgets.forEach(aShifted -> {
            theZIndexToWidget.put(aShifted.getZIndex(), aShifted);
            theIdToZIndex.put(aShifted.getId(), aShifted.getZIndex());
        });
        return aItem;
    }

    @Override
    public Widget update(Widget aItem) {
        LOGGER.debug("Updating {}", aItem);
        Objects.requireNonNull(deleteById(aItem.getId()));
        return create(aItem);
    }

    @Override
    @Nullable
    public Widget deleteById(String aId) {
        LOGGER.debug("Deleting id {}", aId);
        Long myDeletedZ = theIdToZIndex.remove(aId);
        if (myDeletedZ == null) {
            return null;
        }
        return theZIndexToWidget.remove(myDeletedZ);
    }

    @Override
    public long highestZIndex() {
        Long myHighestZIndex = theZIndexToWidget.lowerKey(Long.MAX_VALUE);
        return myHighestZIndex == null
                ? 0
                : myHighestZIndex;
    }

    private List<Widget> shiftExistingWidgets(Widget aIncoming) {
        SortedMap<Long, Widget> myWidgetsWithGreaterOrEqualZIndex = theZIndexToWidget.tailMap(aIncoming.getZIndex());
        if (myWidgetsWithGreaterOrEqualZIndex.isEmpty()) {
            return Collections.emptyList();
        }

        Iterator<Map.Entry<Long, Widget>> myIterator = myWidgetsWithGreaterOrEqualZIndex.entrySet().iterator();
        Map.Entry<Long, Widget> myFirstGreaterOrEqual = myIterator.next();
        if (!shiftingRequired(aIncoming, myFirstGreaterOrEqual.getKey())) {
            return Collections.emptyList();
        }

        List<Widget> myShiftedWidgets = new ArrayList<>();
        LOGGER.debug("Shift widget with [id, z] = [{}, {}]",
                myFirstGreaterOrEqual.getValue().getId(),
                myFirstGreaterOrEqual.getKey());
        myShiftedWidgets.add(incrementZIndex(myFirstGreaterOrEqual.getValue()));
        long myPreviousZIndex = myFirstGreaterOrEqual.getKey();
        myIterator.remove();

        while (myIterator.hasNext()) {
            Map.Entry<Long, Widget> myNext = myIterator.next();
            if ((myPreviousZIndex + 1) != myNext.getKey()) {
                break;
            }
            LOGGER.debug("Shift widget with [id, z] = [{}, {}]", myNext.getValue().getId(), myNext.getKey());
            myShiftedWidgets.add(incrementZIndex(myNext.getValue()));
            myPreviousZIndex = myNext.getKey();
            myIterator.remove();
        }
        return myShiftedWidgets;
    }

    private boolean shiftingRequired(Widget aIncoming, long aExisting) {
        return aIncoming.getZIndex() == aExisting;
    }

    private Widget incrementZIndex(Widget aWidget) {
        return  ImmutableWidget.builder()
                .from(aWidget)
                .zIndex(aWidget.getZIndex() + 1)
                .build();
    }
}
