package com.miro.assignment.controller;

import com.miro.assignment.models.Widget;
import com.miro.assignment.models.WidgetRequest;
import com.miro.assignment.services.WidgetService;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(WidgetController.BASE_PATH)
public class WidgetController {
    public static final String BASE_PATH = "/api/widget";
    private final WidgetService theWidgetService;

    public WidgetController(WidgetService aWidgetService) {
        theWidgetService = aWidgetService;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Widget>> getAll() {
        return ResponseEntity.ok(theWidgetService.getAll());
    }

    @GetMapping(value = "/{aId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Widget> getById(@PathVariable("aId") String aId) {
        return ResponseEntity.ok(theWidgetService.getById(aId));
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Widget> create(@RequestBody WidgetRequest aWidget) {
        return ResponseEntity.ok(theWidgetService.create(aWidget));
    }

    @PutMapping(value = "/{aId}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Widget> update(@PathVariable("aId") String aId, @RequestBody WidgetRequest aWidget) {
        Widget myUpdated = theWidgetService.update(aId, aWidget);
        return ResponseEntity.ok(myUpdated);
    }

    @DeleteMapping("/{aId}")
    public ResponseEntity<?> delete(@PathVariable("aId") String aId) {
        theWidgetService.deleteById(aId);
        return ResponseEntity.noContent().build();
    }
}
