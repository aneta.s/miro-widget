package com.miro.assignment;

import com.miro.assignment.util.CurrentDateTimeProvider;
import com.miro.assignment.util.DateTimeProvider;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.IdGenerator;
import org.springframework.util.JdkIdGenerator;

@Configuration
public class WidgetAppConfig {

    @Bean
    public DateTimeProvider currentTimeProvider() {
        return new CurrentDateTimeProvider();
    }

    @Bean
    public IdGenerator idGenerator() {
        return new JdkIdGenerator();
    }
}
